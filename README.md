# Common device configuration for Lenovo K6 (karate)

## Spec Sheet

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa-core 1.4 GHz ARM Cortex-A53 (ARMv7)
Chipset | Qualcomm Snapdragon 430 MSM8937
GPU     | Adreno 505 @ 450 MHz
Memory  | 3/4 GB RAM
Shipped Android Version | 6.0.1
Storage | 16/32 GB
MicroSD | Up to 128GB
Battery | Non-removable Li-Po 4000 mAh battery
Display | 1080x1920 pixels, 5.0 inches (~441 ppi pixel density)
Dimensions | 141.9 x 70.3 x 9.3 mm
Camera  | Primary: 13 MP PDAF, LED-Flash, FHD (Sony IMX219/Samsung S5K3P3) 
Camera (front)	| 8 MP, Fixed-focus, FHD
Fingerprint | Focaltech Fingerprint Sensor (FPC) 1020
Other Sensors | Gravity, Proximity, Light, Vibrator, Gyroscope, Accelerometer

## Device Picture
![Lenovo K6 Power](http://i.imgur.com/lTgLIRg.jpg "Lenovo K6 Power")

## Copyright

```
#
# Copyright (C) 2016 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
